import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LoginPage;

public class LoginTest {

    private LoginPage loginPage = new LoginPage();

    @Test
    public void testOne() {



        driver.get("https://fabrique:fabrique@finance.dev.fabrique.studio/accounts/login/");

//        LoginPage loginPage = new LoginPage(driver);

        // Введите логин и пароль для авторизации
        String username = "admin@admin.ad";
        String password = "admin";

        // Попытка авторизации
//        loginPage.enterEmail(username);
        loginPage.enterPassword(password);

        // Проверка успешной авторизации
        if (driver.getCurrentUrl().contains("finance.dev.fabrique.studio")) {
            System.out.println("Авторизация успешна.");
        } else {
            System.out.println("Ошибка авторизации.");
        }

        driver.quit();
    }
}

