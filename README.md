# Bug Report by Ekaterina Bogdanova




- Автор Екатерина Богданова  ТГ [@itwasabur (https://t.me/itwasabur)](https://gitlab.com/pureizma)
- Gitlab [@pureizma (https://gitlab.com/pureizma)](https://gitlab.com/pureizma)


## User test case

#### Сортировка по сумме
Шаги теста:

    1. Открыть главную странцу
    2. Открыть страницу платежи
    3. Нажать на сортировать по убыванию поля суммы
Ожидаемые результаты:

    1. Страница доступна и отображает корректно
    2. Сортировка возвращает суммы в порядке убывания
    
Результаты теста:
    
    1. Сортировка выводит некорректные результаты о сумме, например --1000000

Bug Report:

    1. Сумма не учитывает двойные минусы при сортировке, из-за сортировка не выводит корректные значения

#### Проверка страницы добавить платеж


Шаги теста:

    1. Открыть главную страницу
    2. Нажать на кнопку "Добавить платеж"
    3. Ввести информацию о платеже
    4. Ввести отрицательные значения в поле "сумма план" и сумма "факт"
Ожидаемые результаты:

    1. Страница "добавить платеж "доступна и отображает данные корректно
    2. Поле сумма план выведит предупреждение о вводе отрицательного числа
    
Результаты теста:
    
    1. Поле "сумма план" не вывела сообщения о валидации

Bug Report:

    1. Страница добавить платеж не проходит валидацию значений на отрицательные числа

#### Проверка изменений личного кабинета

Шаги теста:

    1. Нажать на главную страницу
    2. Перейти на страницу личный кабинет, нажав на аватарку пользователя
    3. Нажать на кнопку редактировать
    4. Ввести данные о фамилии и имени
    5. Нажать на кнопку сохранить
    6. Перейти на главную страницу
    7. Вернуться на страницу личный кабинет
    8. Увидить изменения в личных данных

Ожидаемые результаты:
    
    1. Переход на страницу добавить платежи работает
    2. Переход на страницу личный кабинет работает
    3. Кнопка сохранить изменяет результаты о фамилии и имени

Результаты теста:
    
    1. Кнопка "сохранить" не работает
    2. Поля "имя", "фамилия" не изменились


Bug Report

    1. К кнопке "сохранить не привязана никакая функция", просто статичная кнопка <buttom>

#### Проверка изменений в страницы Юр. Лица

Шаги теста:

    1. Перейти на главную страницу под аккаунтом с ролью - "Главный администратор"
    2. Перейти на страницу "Юр.лица"
    3. Нажать на любое юр.лицо
    4. Поменять поля "Название" и "Имя"
    5. Нажать на кнопку обновить
    6. Посмотреть применилось ли обновление

Ожидаемый результат:

    1. Переход на страницу Юр.Лица работает корректо
    2. Кнопка обновить работает корректно
    3. Поля "имя","название" изменились

Результаты теста:

    1. Данные корректно изменияются
    2. Переход на страницу юр.лица работает корректно

#### Проверка изменения пароля пользователям


Шаги теста:

    1. Перейти на главную страницу
    2. Перейти на страницу изменить пользователя
    3. Нажать на пользователя с ID = 2
    4. Ввести некоректные данные в поля "логин", "новый пароль"
    5. Ввести новые корретные данные в поля "логин", "новый пароль" и "группа"
    6. Нажать на кнопку обновить
    7. Перейти на страницу пользователи
    8. Посмотреть изменения

Ожидаемый результат:

    1. Переход на страницу пользователи работает корректно
    2. Переход на страницу изменить пользователя работает корректно
    3. Данные корректно вводятся
    4. Происходит валидации  значений

Результаты:
    
    1. Переход на страницу пользователи работает корректно
    2. Переход на страницу изменить пользователя работает корректно
    3. Данные корректно вводятся
    4. Происходит валидации  значений


#### Проверка валидации даты сортировки

Шаги теста:

    1. Перейти на главную страницу
    2. Ввести в пункте сортировка по дате ввести результ с датой "от" большей чем дата "до"
    3. Просмотреть результат

Ожидаемый результат:
    
    1. Переход на главную страницу работает корректно
    2. Происходит валидация значений по дате от и до (не может от быть больше до)
    3. Вернется к значениям по умолчанию

Результат:

    1. Переход на главную страницу работает корректно
    2. Не происходит валидации значений от и до по дате
    3. Выводится некоректная сортировка

BugReport:

    1. Необходимо поправить валидацию значений от и до по дате (не может дата от быть больше до)
    2. Необходимо добавить значения по умолчанию в случаи "ловли" таких значений