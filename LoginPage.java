package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
//    private WebDriver driver;

    @FindBy(xpath = "//input[contains(@placeholder, 'Электронная почта')]")
    private WebElement emailInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(className = "button__content")
    private WebElement loginButton;

    public LoginPage() {
//        this.driver = driver;
    }

    // Методы для взаимодействия с элементами страницы
    public void enterEmail(String email) {
        emailInput.click();
        emailInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }
}

public void init(final WebDriver driver) {
    PageFactory.initElements(new ExtendedFieldDecorator(driver), this);
}